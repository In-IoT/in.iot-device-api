## In.IoT Device API

In.IoT is a middleware platform for Internet of Things (IoT) from an ongoing research project at the Inatel IoT Research Group. It is a contribution that represents a new concept of connecting IoT that is simple to deploy, use, and share.

This is the REST API for the Device Service.

[You can find the user guide here](https://bitbucket.org/In-IoT/in.iot-device-api/wiki/Usage%20Guide)

[You can find installation instructions in our website](https://inatel.br/in-iot/)

Or in our Wiki:

* [Ubuntu](https://bitbucket.org/In-IoT/moquette-in.iot/wiki/installation%20(Ubuntu))
* Windows (Comming soon)
* MAC (Comming soon)
* CentOS (Comming soon)

[To secure your deployment and scale the solution with Microservices access this tutorial](https://bitbucket.org/In-IoT/moquette-in.iot/wiki/scaling%20and%20securing%20the%20solution)
[To configure your MQTT cluster access this tutorial](https://bitbucket.org/In-IoT/moquette-in.iot/wiki/Configuring%20Multiple%20MQTT%20Brokers)


## Ports 

| Application   				| Port 			|
| ------------- 				| ------------- |
| Device API  					| 8070  		|
| Admin GUI						| 8100  		|
| Admin REST API  				| 8190  		|
| MQTT Broker  					| 1883  		|
| Eureka Service discovery  	| 8761  		|
| Zuul Gateway  				| 8090  		|
| MQTT Proxy  					| 5000  		|
| CoAP Server  					| 5683  		|
| CoAP Proxy  					| 5001  		|

