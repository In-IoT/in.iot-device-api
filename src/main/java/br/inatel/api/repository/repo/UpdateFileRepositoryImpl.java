package br.inatel.api.repository.repo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import br.inatel.api.model.UpdateFile;


//import br.inatel.dashboard.model.Message;

public class UpdateFileRepositoryImpl implements  UpdateFileRepositoryCustom {


	  private MongoTemplate mongoTemplate;

	  @Autowired
	  public UpdateFileRepositoryImpl(MongoTemplate mongoTemplate) {
	    this.mongoTemplate = mongoTemplate;
	  }

	  @Override
	  public List<UpdateFile> findByModel(String model, int nResults) {
		  
		  Query query = new Query();
		  
		  query.limit(nResults);
		  query.with(new Sort(Sort.Direction.DESC, "softwareVersion"));
		  query.fields().exclude("model");
		  query.fields().exclude("file");
		  query.addCriteria(new Criteria()
				  
				  .andOperator(
				  
				  Criteria.where("model").is(model)
				  //Criteria.where("events."+mapId).exists(true))

				  ));
		  
		  
		  return mongoTemplate.find(query, UpdateFile.class, "updateFiles");
		  //return mongoTemplate.find(query, DocumentClass.class, "DocumentName");

	    
	  }
	  
	}
