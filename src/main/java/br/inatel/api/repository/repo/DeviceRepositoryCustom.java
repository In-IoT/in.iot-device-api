package br.inatel.api.repository.repo;

import java.util.List;

import br.inatel.api.model.Device;


public interface DeviceRepositoryCustom {

	List<Device> findByAbilityAndOrder(String ability, String order, int nResults);
	
}