package br.inatel.api.repository.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.inatel.api.model.UpdateFile;

//import org.springframework.data.mongodb.repository.MongoRepository;
//import org.springframework.stereotype.Repository;
//
//import br.inatel.dashboard.model.Message;

@Repository("updateFileRepository")
public interface UpdateFileRepository extends MongoRepository<UpdateFile, String>, UpdateFileRepositoryCustom {

} 