package br.inatel.api.repository.repo;

import java.util.List;

import br.inatel.api.model.UpdateFile;

//import br.inatel.dashboard.model.Message;

public interface UpdateFileRepositoryCustom {

	List<UpdateFile> findByModel(String model, int nResults);
	
}