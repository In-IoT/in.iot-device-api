package br.inatel.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.inatel.api.model.EventVariables;

@Repository("trainingMapRepository")
public interface EventVariablesRepository extends MongoRepository<EventVariables, String> {


}
