package br.inatel.api.repository;

import br.inatel.api.model.Group;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository("groupRepository")
public interface GroupRepository extends MongoRepository<Group, String> {

    public Group findByName(String name);
}
