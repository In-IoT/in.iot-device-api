package br.inatel.api.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import br.inatel.api.model.Message;

public interface MessageRepository extends MongoRepository<Message, String> {

    @Query(fields="{created_at:1,events:1,_id:0}")
    Message findByUsername(String username);
    
    List<Message> findAllByUsername(String username);
}
