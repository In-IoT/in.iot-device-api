package br.inatel.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import br.inatel.api.model.Device;

@Repository("deviceRepository")
public interface DeviceRepository extends MongoRepository<Device, String> {

    Optional<Device> findByUsername(String username);
    
    @Query(value="{username : ?0}", fields="{ _id : 0}")
    Optional<Device> findByUsernameLogin(String username);
    
    @Query(fields="{ _id : 0, password : 0, description : 0, publicDevice: 0, "
    		+ "deleted: 0, createDate : 0, application: 0}")
    List<Device> findByAbilitiesAndApplication(String abilities, String application);
}
