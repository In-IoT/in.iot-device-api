package br.inatel.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.inatel.api.model.AutoRegister;


@Repository("autoRegisterRepository")
public interface AutoRegisterRepository extends MongoRepository<AutoRegister, String> {
	
	AutoRegister findByUsernameAndPassword(String username, String password);
	
	AutoRegister findByUsername(String username);

}
