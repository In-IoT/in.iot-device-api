package br.inatel.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inatel.api.model.EventVariables;
import br.inatel.api.repository.EventVariablesRepository;

@Service("trainingMapService")
public class EventVariablesService {

    private EventVariablesRepository trainingMapRepository;

    @Autowired
    public EventVariablesService(EventVariablesRepository trainingMapRepository) {
        this.trainingMapRepository = trainingMapRepository;
    }
    
    
    public void saveMap(EventVariables train) {
    	
    	Optional<EventVariables> trainAux = trainingMapRepository.findById(train.getId());
    	
    	if(trainAux.isPresent())
    		train.getVariables().putAll(trainAux.get().getVariables());
    	trainingMapRepository.save(train);
    	    	
    }
    

}
