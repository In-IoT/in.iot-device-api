package br.inatel.api.service;

import br.inatel.api.model.Device;
import br.inatel.api.model.DeviceList;
import br.inatel.api.repository.DeviceRepository;
import br.inatel.api.repository.repo.DeviceRepositoryImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("deviceService")
public class DeviceService {

    private DeviceRepository deviceRepository;
    
    @Autowired
    DeviceRepositoryImpl deviceRepository2;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    public List<Device> getAllUser() {
        return deviceRepository.findAll();
    }
    
    public Optional<Device> getDevice(String username){
        return deviceRepository.findByUsername(username);
    }

//	public DeviceList findAbility(String deviceAbility, String application) {
//
//		DeviceList deviceList = new DeviceList();
//		List<Device> devices = deviceRepository.findByAbilitiesAndApplication(deviceAbility, application);
//		deviceList.setDevices(devices);
//		return deviceList;
//	}
	
	public Device findAbilityAndOrder(String deviceAbility, String order) {

		DeviceList deviceList = new DeviceList();
		List<Device> devices = deviceRepository2.findByAbilityAndOrder(deviceAbility, order, 30);
		deviceList.setDevices(devices);
		if(deviceList.getDevices().isEmpty())
			return null;
		return deviceList.getDevices().get(0);
	}
}
