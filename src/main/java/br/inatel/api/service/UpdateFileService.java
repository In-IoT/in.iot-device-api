package br.inatel.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.inatel.api.model.UpdateFile;
import br.inatel.api.repository.repo.UpdateFileRepository;

@Service("updateFileService")
public class UpdateFileService {
	
	@Autowired
	private UpdateFileRepository demoDocumentRepository;
	
	
//	@Autowired
//	public UpdateFileService(UpdateFileRepository  demoDocumentRepository) {
//		this.demoDocumentRepository =demoDocumentRepository;
//	}
//	
//	public void save(UpdateFile demoDocument) {
//		demoDocumentRepository.save(demoDocument);
//	}
//	
//	
//	public List<UpdateFile> showDemoDocuments() {
//		return demoDocumentRepository.findAll();
//	}
	
	public List<UpdateFile> findByModel(String model) {
		
		
		// Service
//		PageRequest request = new PageRequest(0, 1, new Sort(Sort.Direction.DESC, "approval.approvedDate"));
//		demoDocumentRepository.getLatestByModelOrderBYVersionNumber(model, request).getContent().get(0);
		
//		PageRequest request = new PageRequest(0, 1, new Sort(Sort.Direction.DESC, "softwareVersion"));
//		UpdateFile updateFile = demoDocumentRepository.findByModel(model,request).get(0);
		//Job job = repository.findOneActiveOldest(request).getContent().get(0);
//		return demoDocumentRepository.findByModel(model, new Sort(Sort.Direction.DESC, "softwareVersion"));
		return demoDocumentRepository.findByModel(model, 1);
	}
	
	public Optional<UpdateFile> findById(String id) {
		return demoDocumentRepository.findById(id);
	}
	
}
