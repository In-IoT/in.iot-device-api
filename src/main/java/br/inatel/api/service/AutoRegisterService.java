package br.inatel.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inatel.api.model.AutoRegister;
import br.inatel.api.repository.AutoRegisterRepository;

@Service("autoRegisterService")
public class AutoRegisterService {
	
	private AutoRegisterRepository autoRegisterRepository;

    @Autowired
    public AutoRegisterService(AutoRegisterRepository autoRegisterRepository) {
        this.autoRegisterRepository =autoRegisterRepository;
    }
    
    public AutoRegister findByUsernameAndPassword(String username, String password) {
    	return autoRegisterRepository.findByUsernameAndPassword(username, password);
    }
    
    public AutoRegister findByUsername(String username) {
    	return autoRegisterRepository.findByUsername(username);
    }

}
