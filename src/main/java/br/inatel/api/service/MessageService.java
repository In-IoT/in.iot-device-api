package br.inatel.api.service;

import br.inatel.api.common.ErrorMessages;
import br.inatel.api.exceptions.MessageNotFound;
import br.inatel.api.model.Message;
import br.inatel.api.repository.MessageRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//import org.json.JSONObject;

@Service("messageService")
public class MessageService {

    private MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public List<Message> getAllMessage() {
        return messageRepository.findAll();
    }
    
    public List<Message> getAllMessageByDeviceUsername(String username) {
        return messageRepository.findAllByUsername(username);
        
    }

    public void addNew(Message message) {
        this.messageRepository.insert(message);
    }

    public void deleteMessage(String id) {
        try {
            this.messageRepository.deleteById(id);
        } catch (RuntimeException e) {
            throw new MessageNotFound(ErrorMessages.MESSAGE_NOT_FOUND);
        }
    }

    
//    public Message getDeviceMessage(String deviceName){
//        
//        JSONObject json = new JSONObject();
//        
//        Message message = messageRepository.findByUsername(deviceName);
//
//        return message;
//    }

}
