package br.inatel.api.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.inatel.api.common.ErrorMessages;
import br.inatel.api.exceptions.DeviceAlreadyExistsException;
import br.inatel.api.exceptions.UserNotFoundException;
import br.inatel.api.exceptions.WrongCredentialsException;
import br.inatel.api.model.Credentials;
import br.inatel.api.model.Device;
import br.inatel.api.repository.DeviceRepository;

@Service("authService")
public class AuthenticationService {

    private BCryptPasswordEncoder passwordEncryptor;
    private DeviceRepository deviceRepository;

    public AuthenticationService(BCryptPasswordEncoder passwordEncryptor, DeviceRepository deviceRepository) {
        this.passwordEncryptor = passwordEncryptor;
        this.deviceRepository = deviceRepository;
    }

    public Device signUp(Device device) {
        
    	/*
    	 * 
    	 * 
    	 * É impressão minha ou o código abaixo devia estar no controller?
    	 */
        
        try {
            this.deviceRepository.insert(device);
            return device;
        } catch (RuntimeException e) {
            throw new DeviceAlreadyExistsException(ErrorMessages.USER_ALREADY_EXISTS);
        }
    }

    public Device signIn(Credentials credentials) {

        if (StringUtils.isEmpty(credentials.getUsername())) {
            throw new UsernameNotFoundException("Username is empty");
        }
        
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(credentials.getUsername(), null,
				new ArrayList<>());
        
        authentication.setDetails("iniot");

		SecurityContextHolder.getContext().setAuthentication(authentication);
        Optional<br.inatel.api.model.Device> user = deviceRepository.findByUsername(credentials.getUsername());
        
        if (!user.isPresent()) {
            throw new UsernameNotFoundException(ErrorMessages.USER_NOT_FOUND + " - " + credentials.getUsername());
        }
        authentication = new UsernamePasswordAuthenticationToken(credentials.getUsername(), null,
				new ArrayList<>());
        
        authentication.setDetails(user.get().getApplication());

		SecurityContextHolder.getContext().setAuthentication(authentication);
		user = deviceRepository.findByUsername(credentials.getUsername());
		if (user.isPresent() && this.checkPassword(credentials.getPassword(), user.get().getPassword())) {
          return user.get();
      } else {
          throw new WrongCredentialsException(ErrorMessages.WRONG_CREDENTIALS);
      }
        
    }

    public Device getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Optional<Device> authenticatedUser = this.deviceRepository.findByUsername(auth.getName());

        if (authenticatedUser.isPresent()) {
            return authenticatedUser.get();
        } else {
            throw new UserNotFoundException(ErrorMessages.USER_NOT_FOUND);
        }
    }

    private boolean checkPassword(String inputPassword, String encryptedPassword) {
        return this.passwordEncryptor.matches(inputPassword, encryptedPassword);
    }
}
