package br.inatel.api.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "autoRegister")
public class AutoRegister {

    @Id
    private String id;
    private String username;
    private String password;
    private String createdBy;
    private String application;
    private String description;
    private Date createdDate;
    private Date expirationDate;
    private List<String> abilities;
    private List<String> keywords;
    private String model;
    private Object abilityDetails;
    private List<String> variables;
    

    public AutoRegister() {

    }

    public AutoRegister(
            @JsonProperty("id") String id,
            @JsonProperty("username") String username,
            @JsonProperty("password") String password,
            @JsonProperty("createdBy") String createdBy,
            @JsonProperty("createdDate") Date createdDate,
            @JsonProperty("expirationDate") Date expirationDate,
            @JsonProperty("application") String application,
            @JsonProperty("description") String description,
            @JsonProperty("abilities") List<String> abilities,
            @JsonProperty("keywords") List<String> keywords,
            @JsonProperty("model") String model,
            @JsonProperty("abilityDetails") Object abilityDetails,
            @JsonProperty("variables") List<String> variables) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.expirationDate = expirationDate;
        this.application = application;
        this.description = description;
        this.abilities = abilities;
        this.keywords = keywords;
        this.model = model;
        this.abilityDetails = abilityDetails;
        this.variables = variables;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getAbilities() {
		return abilities;
	}

	public void setAbilities(List<String> abilities) {
		this.abilities = abilities;
	}


	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Object getAbilityDetails() {
		return abilityDetails;
	}

	public void setAbilityDetails(Object abilityDetails) {
		this.abilityDetails = abilityDetails;
	}

	public List<String> getVariables() {
		return variables;
	}

	public void setVariables(List<String> variables) {
		this.variables = variables;
	}
	
}
