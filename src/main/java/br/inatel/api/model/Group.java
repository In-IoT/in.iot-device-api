
package br.inatel.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "group")
public class Group {
    
    private String id;
    private String name;
    private List<String> deviceName;
    private boolean deleted;
    private Date createDate;
    
    
    public Group(
            @JsonProperty("id") String id,
            @JsonProperty("name") String name,
            @JsonProperty("deviceName") List<String> deviceName,
            @JsonProperty("deleted") boolean deleted,
            @JsonProperty("createDate") Date createDate
    ) {
        this.id = id;
        this.name = name;
        this.deviceName = deviceName;
        this.deleted = deleted;
        this.createDate = createDate;
    }
    
    public Group(){
    
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(List<String> deviceName) {
        this.deviceName = deviceName;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
