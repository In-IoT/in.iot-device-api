package br.inatel.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "messages")
public class Message {

    @Id
    private String id;
    private String content;
    private Date created_at;
    private String username;
    private Map<String, String> events;

    public Map<String, String> getEvents() {
        return events;
    }

    public void setEvents(Map<String, String> events) {
        this.events = events;
    }

    public Message() {

    }
    
    public Message( Map<String, String> events,Date created_at,String username) {
    	this.events = events;
        this.created_at = created_at;
        this.username = username;
    }

    public Message(
            @JsonProperty("id") String id,
            @JsonProperty("content") String content,
            @JsonProperty("created_at") Date created_at,
            @JsonProperty("username") String username) {
        this.id = id;
        this.content = content;
        this.created_at = created_at;
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
