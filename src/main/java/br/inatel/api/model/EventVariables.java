package br.inatel.api.model;

import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "eventVariables")
public class EventVariables {
	
	private String id;
	private Map<String, Boolean> variables;
	
	public EventVariables(){
		
	}
	
	public EventVariables(String id, Map<String, Boolean> variables){
		this.id = id;
		this.variables = variables;
	}

	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Map<String, Boolean> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, Boolean> variables) {
		this.variables = variables;
	}
	
//	@Override
//	public boolean equals(Object o) {
//		if(o==this) return true;
//		if(!(o instanceof EventVariables)) {
//			return false;
//		}
//		EventVariables eventVariables = (EventVariables) o;
//		//return event
//	}

}
