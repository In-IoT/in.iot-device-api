package br.inatel.api;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

@SpringBootApplication
@EnableEurekaClient
//@EnableAsync

public class RunDeviceAPI extends SpringBootServletInitializer {
 
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(RunDeviceAPI.class);
    }
    
//    @Autowired
//    private EurekaClient discoveryClient;
 
    public static void main(String[] args) {
    	checkIfConfigFileExists();
        SpringApplication.run(RunDeviceAPI.class, args);
    }
    
//    @Bean
//    public Executor asyncExecutor() {
//      ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
//      executor.setCorePoolSize(2);
//      executor.setMaxPoolSize(5);
//      executor.setQueueCapacity(500);
//      executor.initialize();
//      return executor;
//    }
    
//    @Bean
//    public ServletWebServerFactory servletContainer(@Value("${server.port}") int httpPort) throws UnknownHostException {
//        
//    	Connector connector = new Connector(TomcatServletWebServerFactory.DEFAULT_PROTOCOL);
//        connector.setPort(8000);
//        //connector.setAttribute("address", InetAddress.getByName(httpHost).getHostAddress());
//        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
//        tomcat.addAdditionalTomcatConnectors(connector);
//        return tomcat;
//    }
    
    private static void checkIfConfigFileExists() {

		String filename = System.getProperty("user.dir")+ File.separator + "config" + File.separator + "application.properties";
		File file = new File(filename);
		if (!file.exists()) {
			file.mkdirs();
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try (Writer writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(filename), "utf-8"))) {
				writer.write(""
						+ "logging.level.org.springframework.web: WARN\n" + 
						"logging.level.org.hibernate: WARN\n" + 
						"\n" + 
						"server.port = 8070\n" + 
						"spring.application.name= device-api\n" + 
						"spring.cloud.refresh.enabled=false\n" + 
						"  \n" + 
						"#eureka.client.serviceUrl.defaultZone=${EUREKA_URI:http://localhost:8761/eureka}\n" + 
						"eureka.client.serviceUrl.defaultZone=http://inIoTTest:lucasabbadeWare@localhost:8761/eureka/" +
						"\neureka.instance.preferIpAddress= true\n" + 
						"\n" + 
						"\n" + 
						"#If eureka and Zuul are disabled, these two options should be set to false\n" + 
						"eureka.client.register-with-eureka=false\n" + 
						"eureka.client.fetch-registry=false\n" + 
						"\n" + 
						"jwt.secret=SecretKeyToGenJWTsDEVICE" + 
						"\n" + 
						"\n" + 
						"\n" + 
						"#example of mongoDB Running on localhost in port 27017 and database iniot\n" + 
						"#MONGODBURL = mongodb://localhost:27017\n" + 
						"#MONGODATABASE = iniot\n" + 
						"\n" + 
						"\n" + 
						"#example of mongoDB Running on mongodb cloud server and database iniot\n" + 
						"#mongodb+srv://iotlab:316S7HKjvFXiSHkvdWiAee@cluster0-065g.mongodb.net\n" + 
						"#MONGODATABASE = iniot");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

//@SpringBootApplication
//public class RunAPI {
//
//	public static void main(String[] args) {
//		SpringApplication.run(RunAPI.class, args);
//	}
//}


