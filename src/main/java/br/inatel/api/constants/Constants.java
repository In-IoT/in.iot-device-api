package br.inatel.api.constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Constants {
	
	public static final String MONGOURL; //= "mongodb://localhost:27017";
    
    public static final String MONGODATABASE; //= "springmongodb";
    public static final String SECRETDEVICE;

	static {
		InputStream inputStream = null;
		Properties prop = new Properties();
		String propFileName = "application.properties";
		
		try {
			inputStream = new FileInputStream (System.getProperty("user.dir")+ File.separator + "config" + File.separator + "application.properties");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String secret = prop.getProperty("jwt.secret", "SecretKeyToGenJWTsDEVICE");

		SECRETDEVICE = secret;
    	MONGOURL = prop.getProperty("MONGODBURL", "mongodb://localhost:27017");
    	MONGODATABASE = prop.getProperty("MONGODATABASE", "iniot");
    	
    }
	


}
