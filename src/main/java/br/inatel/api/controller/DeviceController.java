package br.inatel.api.controller;

import static br.inatel.api.security.SecurityConstants.CONTROLLER_URL_DEVICE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.inatel.api.model.Device;
import br.inatel.api.model.DeviceList;
import br.inatel.api.service.DeviceService;

@RestController
@RequestMapping(CONTROLLER_URL_DEVICE)
public class DeviceController {

    private DeviceService deviceService;

    @Autowired
    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Device> getAllUser() {
        return this.deviceService.getAllUser();
    }
    
//    @RequestMapping(value = "/findAbility"+"/{deviceAbility}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
//	public DeviceList findAbility(@PathVariable(name = "deviceAbility", required = true) String deviceAbility) {
//
//    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//    	String application = auth.getDetails().toString();
//    	return this.deviceService.findAbility(deviceAbility, application);
//	}
    
    @RequestMapping(value = "/findAbility"+"/{deviceAbility}/{order}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Device findAbility(@PathVariable(name = "deviceAbility", required = true) String deviceAbility,
			@PathVariable(name = "order", required = true) String order) {

    	return this.deviceService.findAbilityAndOrder(deviceAbility, order);
    }
}
