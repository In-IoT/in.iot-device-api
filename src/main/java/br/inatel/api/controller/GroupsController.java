
package br.inatel.api.controller;

import br.inatel.api.model.Group;
import br.inatel.api.service.GroupService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class GroupsController {
    
    private GroupService groupsService;

    @Autowired
    public GroupsController(GroupService groupsService) {
        this.groupsService = groupsService;
    }
    
    public List<Group> getAllGroups() {
        return this.groupsService.getAllGroups();
    }
    
    public Group findByName(String name){
        return this.groupsService.getGroup(name);
    }
    
}
