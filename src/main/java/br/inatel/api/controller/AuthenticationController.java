package br.inatel.api.controller;

import static br.inatel.api.constants.Constants.MONGODATABASE;
import static br.inatel.api.security.SecurityConstants.CONTROLLER_AUTH_URL_DEVICE;
import static br.inatel.api.security.SecurityConstants.EXPIRATION_TIME;
import static br.inatel.api.security.SecurityConstants.SECRETDEVICE;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.inatel.api.model.AutoRegister;
import br.inatel.api.model.Device;
import br.inatel.api.service.AuthenticationService;
import br.inatel.api.service.AutoRegisterService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Signin endpoint is not necessary here because JWTAuthenticationFilter handles
 * the /api/v1/auth/signin endpoint. Signin flow happens there
 */
@RestController
@RequestMapping(CONTROLLER_AUTH_URL_DEVICE)
public class AuthenticationController {

	private AuthenticationService authService;
	private AutoRegisterService autoRegisterService;
	private BCryptPasswordEncoder passwordEncryptor;

	public AuthenticationController(AuthenticationService authService, AutoRegisterService autoRegisterService, BCryptPasswordEncoder passwordEncryptor) {
		this.authService = authService;
		this.autoRegisterService = autoRegisterService;
		this.passwordEncryptor = passwordEncryptor;
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String signUp(@RequestBody AutoRegister autoRegisterOriginal) {

		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
				autoRegisterOriginal.getUsername(), null, new ArrayList<>());
		authentication.setDetails(MONGODATABASE);

		SecurityContextHolder.getContext().setAuthentication(authentication);
		AutoRegister autoRegister = autoRegisterService.findByUsername(autoRegisterOriginal.getUsername());

		if (autoRegister == null)
			return "Username and Password was not found";
		
		
		authentication = new UsernamePasswordAuthenticationToken(autoRegisterOriginal.getUsername(), null, new ArrayList<>());
		authentication.setDetails(MONGODATABASE);
		SecurityContextHolder.getContext().setAuthentication(authentication);

		autoRegister = autoRegisterService.findByUsernameAndPassword(autoRegisterOriginal.getUsername(), autoRegisterOriginal.getPassword());

		if (autoRegister == null)
			return "Username and Password was not found";
		
		Device device = new Device();
		device.setCreateDate(new Date());
		device.setApplication(autoRegister.getApplication());
		device.setCreateBy(autoRegister.getCreatedBy());

		String username = UUID.randomUUID().toString();
        device.setUsername(username);

        this.authService.signUp(device);
        
        device.setDeleted(false);
		device.setPublicDevice(true);
        device.setDescription(autoRegisterOriginal.getDescription());
        device.setAbilities(autoRegisterOriginal.getAbilities());
        device.setKeywords(autoRegisterOriginal.getKeywords());
        device.setModel(autoRegisterOriginal.getModel());
        device.setAbilityDetails(autoRegisterOriginal.getAbilityDetails());
        device.setVariables(autoRegisterOriginal.getVariables());
        String password = PasswordGenerator.generatePassword();
        device.setPassword(passwordEncryptor.encode(password));

		
		
		authentication = new UsernamePasswordAuthenticationToken(autoRegister.getUsername(), null, new ArrayList<>());
		authentication.setDetails(autoRegister.getApplication());
		SecurityContextHolder.getContext().setAuthentication(authentication);
		this.authService.signUp(device);

		device.setPassword(null);
		
		return "{\"username\": \"" + device.getUsername() + "\"," + "\"password\": \"" + password+ "\"}";

		/*
		 * 
		 * Aqui depois tem que ver como retornar o código de erro ele retorna sempre o
		 * código de created...
		 */
		
	}

	@RequestMapping(value = "/me", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Device getAuthenticatedUser() {
		Device device = this.authService.getAuthenticatedUser();
		device.setPassword(null);
		return device;
	}

	public void teste() {
		String token = Jwts.builder().setSubject("user")
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS256, SECRETDEVICE.getBytes())

				.compact();

	}
}
