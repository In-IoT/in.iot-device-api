package br.inatel.api.controller;

import static br.inatel.api.security.SecurityConstants.CONTROLLER_URL_MESSAGE;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.google.common.base.Functions;
import com.google.common.collect.Maps;

import br.inatel.api.model.EventVariables;
import br.inatel.api.model.Message;
import br.inatel.api.service.EventVariablesService;
//import br.inatel.api.service.GroupService;
import br.inatel.api.service.MessageService;

@RestController
@RequestMapping(CONTROLLER_URL_MESSAGE)
public class MessageController {

	private MessageService messageService;
	//private TestJSONService testJSONService;
	@Autowired
	private EventVariablesService trainingMapService;

	@Autowired
	public MessageController(MessageService messageService) {//, TestJSONService testJSONService,
			//EventVariablesService trainingMapService) {
		this.messageService = messageService;
		//this.testJSONService = testJSONService;
		//this.trainingMapService = trainingMapService;
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Message> getAllMessage() {

		String device = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

		return messageService.getAllMessageByDeviceUsername(device);
		// return this.messageService.getAllMessage();
	}
	
//	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	public TestJSON getAllMessage() {
//
////		String device = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
////		//TestJSON test = testJSONService.returnThings().get();
////		TestJSON test = new TestJSON();
////		//test.setJsonObj(jsonObj);
////		System.out.println(test.getId());
////		System.out.println(test.getPassword());
////		System.out.println(test.getUsername());
////		System.out.println(test.getJsonObj());
////		JSONObject jsonObj = new JSONObject();
////		jsonObj.append("test", "value");
////		test.setJsonObj(jsonObj);
////		//test.getJsonObj().
////		//returnThin
//		return testJSONService.returnThings().get();
//		//return null;
//		//return testJSONService.returnThings();
//		// return this.messageService.getAllMessage();
//	}
	
//	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
//	//@ResponseStatus(HttpStatus.CREATED)
//	public void test( @RequestBody Map<String,Object> jsonObj) {
//		
//		//System.out.println(object.get("username"));
//		//private JSONObject jsonSettings;
//		
//		Gson gson = new Gson();
//		//String json = gson.toJson(obj); 
//		//JSONObject jsonObj = new JSONObject(jsonString);
//		jsonObj.remove("username");
//		jsonObj.remove("password");
//		//jsonObj.append("user", "value");
//		System.out.println(jsonObj);
//		
//		TestJSON test = new TestJSON();
//		test.setEvents(jsonObj);
//		test.setUsername("testing");
//		test.setPassword("test2");
//		
//		testJSONService.save(test);
//
//	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public void addNew( @RequestBody Map<String, String> object) {
		
		
		System.out.println("got here");


//		if (object.size() == 0)
//			throw new BodyNotFoundException(ErrorMessages.NO_VARIABLES_PROVIDED);

		String user = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		
		Message message = new Message(object, new Date(), user);
//		message.setCreated_at(new Date());
//		message.setEvents(object);

		

//		message.setUsername(user);

		messageService.addNew(message);
		Map<String, Boolean> newMap = Maps.newHashMap(Maps.transformValues(object, Functions.constant(true)));
		EventVariables eventVariables = new EventVariables(user, newMap);
		trainingMapService.saveMap(eventVariables);
		//System.out.println("1");
		
//
//		//System.out.println("2");
		
//		EventVariables eventVariables = new EventVariables(user, object);
//
		
//		Message message = new Message();
//
//		message.setCreated_at(new Date());
//		message.setEvents(object);
//
//
//		String application = RequestContextHolder.getRequestAttributes().getAttribute("tenantId", RequestAttributes.SCOPE_SESSION).toString();
//		//String application = SecurityContextHolder.getContext().getAuthentication().getDetails().toString();
//		
//		//System.out.println("Application " + application);
//		message.setUsername(user);
//
//		messageService.addNew(message);
		
//		SecurityContext context = SecurityContextHolder.getContext();
////		
//		RequestAttributes attributes = RequestContextHolder.currentRequestAttributes();
//		System.out.println("req "+attributes.getAttributeNames(RequestAttributes.SCOPE_REQUEST));
//		System.out.println("session "+attributes.getAttributeNames(RequestAttributes.SCOPE_SESSION));
		//System.out.println("req "+attributes.getAttributeNames(RequestAttributes.));
		
//		RequestContextHolder.setRequestAttributes(attributes, true);
		/*
		try {
			CompletableFuture<EventVariablesService> comp = asyncMethod(object, user);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/

		
//		new Thread(new Runnable() {
//	        public void run(){
//	        	asyncMethod(object, user, context, attributes);
//	        }
//	    }).start();
		//System.out.println("end");
		/*
		 * EventVariables eventVariables = new EventVariables();
		 * 
		 * Map<String, Boolean> newMap = Maps.newHashMap( Maps.transformValues(object,
		 * Functions.constant(true)));
		 * 
		 * 
		 * 
		 * eventVariables.setId(user);
		 * 
		 * eventVariables.setVariables(newMap);
		 * 
		 * trainingMapService.saveMap(eventVariables);
		 * 
		 */
		// System.out.println("END");
		
		//RequestAttributes.

	}

	public void asyncMethod(Map<String, String> object, String user,
			SecurityContext context,
			RequestAttributes attributes
			) {

		SecurityContextHolder.setContext(context);
		//context.getAuthentication().
		//SecurityContextHolder.
		RequestContextHolder.setRequestAttributes(attributes, true);
		
		//ThreadLocal.remove();
//		System.out.println("User 1: " + user);
//		user = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
//		System.out.println("User 2: "+user);
//		user = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
//		SecurityContextHolder.setContext(context);
//		RequestContextHolder.setRequestAttributes(attributes, true);
//		System.out.println("Show id " + RequestContextHolder.getRequestAttributes().getAttribute("tenantId",
//				RequestAttributes.SCOPE_REQUEST));
//		System.out.println("User " + SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
//		//System.out.println("App :(" +application);
//		RequestContextHolder.getRequestAttributes().setAttribute("tenantId", application,
//				RequestAttributes.SCOPE_SESSION);
//		RequestContextHolder.getRequestAttributes().setAttribute("tenantId", application,
//				RequestAttributes.SCOPE_SESSION);
//		SecurityContextHolder.getContext().getAuthentication().getDetails().
//		RequestContextHolder.getRequestAttributes().setAttribute("tenantId", application,
		//RequestAttributes.SCOPE_REQUEST);
		
		//Message message = new Message();

		Message message = new Message(object, new Date(), user);
//		message.setCreated_at(new Date());
//		message.setEvents(object);

		

//		message.setUsername(user);

		messageService.addNew(message);
//
//		//System.out.println("1");
		Map<String, Boolean> newMap = Maps.newHashMap(Maps.transformValues(object, Functions.constant(true)));
//
//		//System.out.println("2");
		EventVariables eventVariables = new EventVariables(user, newMap);
//		EventVariables eventVariables = new EventVariables(user, object);
//
		trainingMapService.saveMap(eventVariables);
//		//System.out.println("5");
//		// CompletableFuture.completedFuture(message);
		//Thread.currentThread().
//		SecurityContextHolder.clearContext();
//		RequestContextHolder.resetRequestAttributes();
//		try {
//			Thread.currentThread().interrupt();
//			finalize();
//			//HttpSession.invalidate();
//		} catch (Throwable e) {
//			// TODO Auto-generated catch block
//			//e.printStackTrace();
//		}
	}

	/*
	 * @Async public CompletableFuture<EventVariablesService>
	 * asyncMethod(Map<String, String> object, String user) throws
	 * InterruptedException {
	 * 
	 * 
	 * EventVariables eventVariables = new EventVariables();
	 * 
	 * Map<String, Boolean> newMap = Maps.newHashMap( Maps.transformValues(object,
	 * Functions.constant(true)));
	 * 
	 * 
	 * 
	 * eventVariables.setId(user);
	 * 
	 * eventVariables.setVariables(newMap);
	 * 
	 * trainingMapService.saveMap(eventVariables);
	 * //CompletableFuture.completedFuture(message); Thread.sleep(5000L);
	 * System.out.println("BEFORE ENDED"); return null; }
	 */

//	@RequestMapping(value = "/specific/{group}/{device}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public ResponseEntity getSpecificMessage(@PathVariable("group") String groupName,
//			@PathVariable("device") String deviceName) {
//
//		String user = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
//		Group group;
//		List<String> names;
//		Message message;
//
//		try {
//			group = groupService.getGroup(groupName);
//			names = group.getDeviceName();
//			if (names.contains(user)) {
//				message = getMessageDevice(deviceName);
//				return new ResponseEntity(message, HttpStatus.OK);
//			}
//		} catch (Exception err) {
//			String messagem = "{\n\"Invalid\": \"No data\"\n}";
//			return new ResponseEntity(messagem, HttpStatus.NOT_ACCEPTABLE);
//		}
//
//		return null;
//	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteMessage(@PathVariable("id") String id) {
		this.messageService.deleteMessage(id);
	}

//	private Message getMessageDevice(String deviceName) {
//		return this.messageService.getDeviceMessage(deviceName);
//	}
}
