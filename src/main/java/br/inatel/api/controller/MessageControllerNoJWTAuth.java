package br.inatel.api.controller;

import static br.inatel.api.security.SecurityConstants.CONTROLLER_URL_MESSAGE;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Functions;
import com.google.common.collect.Maps;

import br.inatel.api.common.ErrorMessages;
import br.inatel.api.exceptions.BodyNotFoundException;
import br.inatel.api.exceptions.WrongCredentialsException;
import br.inatel.api.model.Credentials;
import br.inatel.api.model.EventVariables;
import br.inatel.api.model.Message;
import br.inatel.api.service.AuthenticationService;
import br.inatel.api.service.EventVariablesService;
import br.inatel.api.service.MessageService;

@RestController
@RequestMapping(CONTROLLER_URL_MESSAGE)
public class MessageControllerNoJWTAuth {

	private MessageService messageService;
	private AuthenticationService authenticationService;
	private EventVariablesService trainingMapService;

	@Autowired
	public MessageControllerNoJWTAuth(MessageService messageService, AuthenticationService authenticationService, EventVariablesService trainingMapService) {
		this.messageService = messageService;
		this.authenticationService = authenticationService;
		this.trainingMapService = trainingMapService;
	}

	@RequestMapping(value = "/bodyAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	// @ResponseStatus(HttpStatus.CREATED)
	public void bodyAuth(@RequestBody Map<String, String> object) {

		if (object.size() < 3)
			throw new BodyNotFoundException(ErrorMessages.NO_VARIABLES_PROVIDED);

		if (object.get("username") == null || object.get("username").isEmpty() || object.get("password") == null
				|| object.get("password").isEmpty())
			throw new WrongCredentialsException(ErrorMessages.NO_CREDENTIALS_PROVIDED);
		Credentials credentials = new Credentials();
		credentials.setPassword(object.get("password"));
		credentials.setUsername(object.get("username"));

//				+ object.get("password"));

		authenticationService.signIn(credentials);
		object.remove("username");
		object.remove("password");

		Message message = new Message();
		message.setCreated_at(new Date());
		message.setEvents(object);
		message.setUsername(credentials.getUsername());
		messageService.addNew(message);
		
		EventVariables eventVariables = new EventVariables();
        
        Map<String, Boolean> newMap = Maps.newHashMap(
        	    Maps.transformValues(object, Functions.constant(true)));
        
        
        
        eventVariables.setId(credentials.getUsername());
        
        eventVariables.setVariables(newMap);
//        eventVariables.setVariables(object);
        
        trainingMapService.saveMap(eventVariables);

	}

	@RequestMapping(value = "/headerAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	// @ResponseStatus(HttpStatus.CREATED)
	public void addNewWithoutAuth(@RequestBody(required = true) Map<String, String> object,
			@RequestHeader(value = "username") String username, @RequestHeader(value = "password") String password) {


		if (object.size() == 0)
			throw new BodyNotFoundException(ErrorMessages.NO_VARIABLES_PROVIDED);

		Credentials credentials = new Credentials();
		credentials.setUsername(username);
		credentials.setPassword(password);
		
		
		authenticationService.signIn(credentials);

		Message message = new Message();
		message.setCreated_at(new Date());
		message.setEvents(object);
		message.setUsername(credentials.getUsername());
		messageService.addNew(message);
		
		EventVariables eventVariables = new EventVariables();
        
        Map<String, Boolean> newMap = Maps.newHashMap(
        	    Maps.transformValues(object, Functions.constant(true)));
        
        
        
        eventVariables.setId(credentials.getUsername());
        
        eventVariables.setVariables(newMap);
//        eventVariables.setVariables(object);
        
        trainingMapService.saveMap(eventVariables);
		

	}
	
	
	@RequestMapping(value = "/trySomething", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	// @ResponseStatus(HttpStatus.CREATED)
	public void trySomething(HttpServletRequest request) {
		
		System.out.println(request.getContentLength());
		System.out.println(request.getCharacterEncoding());
		System.out.println(request.getAuthType());
		System.out.println(request.getContentLengthLong());
		System.out.println(request.getContextPath());
		System.out.println(request.getContentType());
		System.out.println(request.getLocalAddr());
		System.out.println(request.getLocalName());
		System.out.println(request.getLocalPort());
		System.out.println(request.getMethod());
		System.out.println(request.getPathInfo());
		System.out.println(request.getPathTranslated());
		System.out.println(request.getProtocol());
		System.out.println(request.getQueryString());
		System.out.println(request.getRemoteAddr());
		System.out.println(request.getRemoteHost());
		System.out.println(request.getRemotePort());
		System.out.println(request.getRemoteUser());
		System.out.println(request.getRequestedSessionId());
		System.out.println(request.getRequestURI());
		System.out.println(request.getScheme());
		System.out.println(request.getServerName());
		System.out.println(request.getServerPort());
		System.out.println(request.getServletPath());
		//System.out.println(request.getAsyncContext());
		System.out.println(request.getAttributeNames());
		System.out.println(request.getCookies());
		System.out.println(request.getDispatcherType());
		System.out.println(request.getHeaderNames());
		System.out.println(request.getLocale());
		System.out.println(request.getParameterMap());
		System.out.println(request.getParameterNames());
		//System.out.println(request.getParameterValues(name));
		System.out.println(request.getRequestURL());
		System.out.println(request.getSession());
		System.out.println(request.getUserPrincipal());
		//System.out.print(reques);
		System.out.println(request.getHeader("Host"));

	}

}
