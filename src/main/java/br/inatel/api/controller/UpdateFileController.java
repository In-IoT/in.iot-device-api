package br.inatel.api.controller;

import static br.inatel.api.constants.Constants.MONGODATABASE;
import static br.inatel.api.security.SecurityConstants.CONTROLLER_URL_MESSAGE;
import static br.inatel.api.security.SecurityConstants.EXPIRATION_TIME;
import static br.inatel.api.security.SecurityConstants.SECRETDEVICE;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.google.common.base.Functions;
import com.google.common.collect.Maps;

import br.inatel.api.model.Device;
import br.inatel.api.model.EventVariables;
import br.inatel.api.model.Message;
import br.inatel.api.model.UpdateFile;
import br.inatel.api.service.DeviceService;
import br.inatel.api.service.EventVariablesService;
//import br.inatel.api.service.GroupService;
import br.inatel.api.service.MessageService;
import br.inatel.api.service.UpdateFileService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@RestController
@RequestMapping("/Updates")
public class UpdateFileController {

	@Autowired
	UpdateFileService updateFileService;

	@Autowired
	DeviceService deviceService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String verifyUpdatesAndGetToken(HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		String username = auth.getName();
		String application = auth.getDetails().toString();

		Optional<Device> dev = deviceService.getDevice(username);

//		if(!dev.isPresent())
//			return "no updates available";

		String model = dev.get().getModel();

		
		UpdateFile dev2 = updateFileService.findByModel(model).get(0);
		if (dev2 == null)
			return "no updates available";
		final long EXPIRATION_TIME = 864_000_000;
		String token = Jwts.builder().setSubject(username).setIssuer(application)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME)).setId(dev2.getId())
				.signWith(SignatureAlgorithm.HS256, SECRETDEVICE.getBytes()).compact();

		response.setHeader("token", token);
		return dev2.getSoftwareVersion() +""; 

	}

	@RequestMapping(method = RequestMethod.GET, value = "/update/{token}")
	public String verifyUpdatesAndGetToken(@PathVariable(name = "token", required = false) String token,
			HttpServletResponse response, HttpServletRequest request) {


		try {

			Claims claim = Jwts.parser().setSigningKey(SECRETDEVICE.getBytes())
					// aqui bastaria colocar .parseClaimsJws(token)
					.parseClaimsJws(token).getBody();
			String user = claim.getSubject();

			String application = claim.getIssuer();

			String id = claim.getId();

			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
					user, null, new ArrayList<>());

			usernamePasswordAuthenticationToken.setDetails(application);
			
//			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
//					autoRegisterOriginal.getUsername(), null, new ArrayList<>());
//			authentication.setDetails(MONGODATABASE);

			SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

			Optional<UpdateFile> file = updateFileService.findById(id);

			Binary document = file.get().getFile();
			InputStream myInputStream = new ByteArrayInputStream(document.getData());

			String filename = file.get().getId();
			
			response.setHeader("content-disposition", "attachment; filename=\"" + filename + "\".bin");
			int length = document.getData().length;
			response.setContentType("application/octet-stream");
			response.setContentLength(length);
			response.setContentLengthLong(length);

			try {
				IOUtils.copy(myInputStream, response.getOutputStream());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		} catch (ExpiredJwtException | MalformedJwtException | SignatureException | UnsupportedJwtException
				| IllegalArgumentException e) {
			System.out.println(e);
			return "your token is not valid or is expired";
		}

		return token;

	}

}
