package br.inatel.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MessageNotFound extends RuntimeException {

    private static final long serialVersionUID = -1264383737246111459L;

    public MessageNotFound(String message) {
        super(message);
    }
}
