package br.inatel.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class InvalidTokenException extends RuntimeException {

    private static final long serialVersionUID = 821685717254349575L;

    public InvalidTokenException(String message) {
        super(message);
    }
}
