package br.inatel.api.mongo;


import static br.inatel.api.constants.Constants.MONGODATABASE;

import java.net.UnknownHostException;

import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.security.core.context.SecurityContextHolder;

import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

/**
 * Created by Naveen Babu on 12-08-2016.
 */
public class MultiTenantMongoDbFactory extends SimpleMongoDbFactory {

    //public String DEFAULT_DB = "hh";

    public MultiTenantMongoDbFactory(MongoClientURI uri) throws UnknownHostException {
        super(uri);
    }

    
    @Override
    public MongoDatabase getDb() throws DataAccessException {
 
////    	String user = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
//        // Check the RequestContext
//        String tenant = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

    	if(SecurityContextHolder.getContext().getAuthentication() == null)
    		return super.getDb(MONGODATABASE);
    	String tenant = SecurityContextHolder.getContext().getAuthentication().getDetails().toString();


            return getDb(tenant);
    	
//        String tenant = SecurityContextHolder.getContext().getAuthentication().getDetails().toString();
//        
//
//
//        
//        
////        if (tenant instanceof String)
////        {
//            return getDb(tenant);
////        }

        // Return a default DB
//        return super.getDb(DEFAULT_DB);
    }

}
