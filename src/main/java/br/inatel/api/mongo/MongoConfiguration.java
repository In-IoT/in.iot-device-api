package br.inatel.api.mongo;

import static br.inatel.api.constants.Constants.MONGODATABASE;
import static br.inatel.api.constants.Constants.MONGOURL;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@Configuration
@ComponentScan(basePackages = "br.inatel.api")
@EnableMongoRepositories({"br.inatel.api.repository"})
@EnableReactiveMongoRepositories
public class MongoConfiguration {
//
//    @Autowired
//    MongoMappingContext mongoMappingContext;
//
//    @Bean
//    public MongoDbFactory mongoDbFactory() throws Exception {
//        String url = MONGOURL;
//        MongoClientURI mongoURI = new MongoClientURI(url);
//        MongoClient mongoClient = new MongoClient(mongoURI);
//        return new SimpleMongoDbFactory(mongoClient, MONGODATABASE);
//    }
//
//    @Bean
//    public MongoTemplate mongoTemplate() throws Exception {
//        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory());
//        MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
//        converter.setTypeMapper(new DefaultMongoTypeMapper(null));
//        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory(), converter);
//        return mongoTemplate;
//    }
}
