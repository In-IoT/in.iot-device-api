package br.inatel.api.security;

import static br.inatel.api.security.SecurityConstants.HEADER_STRING;
import static br.inatel.api.security.SecurityConstants.SECRETDEVICE;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
//public class JWTAuthorizationFilter extends OncePerRequestFilter {
	public JWTAuthorizationFilter(AuthenticationManager authManager) {
		super(authManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		
		System.out.println("Got inside this thing");

		String token = req.getHeader(HEADER_STRING); // + ""; // to guarantee that the token is never null
		UsernamePasswordAuthenticationToken authentication = getAuthentication(token);

		// System.out.println("Token " + token);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(String token) {



		try {

			Claims claim = Jwts.parser().setSigningKey(SECRETDEVICE.getBytes())
					// aqui bastaria colocar .parseClaimsJws(token)
					.parseClaimsJws(token).getBody();
			String user = claim.getSubject();

			String application = claim.getIssuer();

//				RequestContextHolder.getRequestAttributes().setAttribute("tenantId", application,
//						RequestAttributes.SCOPE_REQUEST);

			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
					user, null, new ArrayList<>());

			usernamePasswordAuthenticationToken.setDetails(application);
			return usernamePasswordAuthenticationToken;
			// return new UsernamePasswordAuthenticationToken(user, null).na;
//				return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());

		} catch (ExpiredJwtException | MalformedJwtException | SignatureException | UnsupportedJwtException
				| IllegalArgumentException e) {
			System.out.println(e);
		}

//			try {
//				user = Jwts.parser().setSigningKey(SECRETDEVICE.getBytes())
//						// aqui bastaria colocar .parseClaimsJws(token)
//						.parseClaimsJws(token).getBody().getSubject();
//
//			} catch (ExpiredJwtException | MalformedJwtException | SignatureException | UnsupportedJwtException
//					| IllegalArgumentException e) {
//				// System.out.println(e);
//			}
//			if (user != null) {
//
//				application = Jwts.parser().setSigningKey(SECRETDEVICE.getBytes())
//						// aqui bastaria colocar .parseClaimsJws(token)
//						.parseClaimsJws(token).getBody().getIssuer();
//
//				RequestContextHolder.getRequestAttributes().setAttribute("tenantId", application,
//						RequestAttributes.SCOPE_REQUEST);
//
//				return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
//			}
//			return null;
//		}
//		return null;
//	}
		return null;
	}
}
