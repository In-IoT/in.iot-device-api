package br.inatel.api.security;

import static br.inatel.api.security.SecurityConstants.EXPIRATION_TIME;
import static br.inatel.api.security.SecurityConstants.HEADER_STRING;
import static br.inatel.api.security.SecurityConstants.SECRETDEVICE;
import static br.inatel.api.security.SecurityConstants.SIGN_IN_URL_DEVICES;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.util.JSONParseException;

import br.inatel.api.common.ErrorMessages;
import br.inatel.api.exceptions.UserNotFoundException;
import br.inatel.api.model.Credentials;
import br.inatel.api.model.Device;
import br.inatel.api.repository.DeviceRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;
    private DeviceRepository deviceRepository;
    private static final Logger logger = Logger.getLogger("AuthenticationFilter");

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, DeviceRepository deviceRepository) {
        this.authenticationManager = authenticationManager;
        this.deviceRepository = deviceRepository;
        this.setFilterProcessesUrl(SIGN_IN_URL_DEVICES);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
            HttpServletResponse res) throws AuthenticationException {
        logger.info("--- Attempting Authentication ---");
        try {
        	
        	
            Credentials credentials = new ObjectMapper()
                    .readValue(req.getInputStream(), Credentials.class);
            
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            credentials.getUsername(),
                            credentials.getPassword(),
                            new ArrayList<>())
            );
        } catch (IOException | JSONParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
            HttpServletResponse res,
            FilterChain chain,
            Authentication auth) throws IOException {

    	
        String username = ((User) auth.getPrincipal()).getUsername();
        Optional<Device> applicationUser = this.deviceRepository.findByUsernameLogin(username);
        // Building the authentication token & add to ResponseHeader
        String token = Jwts.builder()
                .setSubject(username)
                .setIssuer(applicationUser.get().getApplication())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS256, SECRETDEVICE.getBytes())
                .compact();
        res.addHeader(HEADER_STRING, token);
        logger.info("--- Successful Authentication ---");

        ServletOutputStream output = res.getOutputStream();

        try {
            

            if (applicationUser.isPresent()) {
            	applicationUser.get().setPassword(null);
                output.print(new ObjectMapper().writeValueAsString(applicationUser.get()));
            } else {
                throw new UserNotFoundException(ErrorMessages.USER_NOT_FOUND);
            }
        } finally {
            output.close();
        }
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException failed) throws IOException {
        //ServletOutputStream output = response.getOutputStream();

        response.sendError(HttpStatus.UNAUTHORIZED.value(), ErrorMessages.WRONG_CREDENTIALS);
        //output.print(ErrorMessages.WRONG_CREDENTIALS);
    }
}
